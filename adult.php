<?php 
include("header.php");
//session_destroy();
?>
		<input type="hidden" id="scriptPage" value="adult/adultScript.R" />
		<input type="hidden" id="graphScriptLocation" value="adult/adultGraph" />
		
        <div class="container-fluid">
            <div class="row">
            	<div class="col-lg-1">
            	
           	 	</div>
                <div class="col-lg-10 col-md-offset-0"> 
                	<div class="page-header">
                    	<h2>Adult Data Set</h2>
                		<div class="well">
        					<p>Extraction was done by Barry Becker from the 1994 Census database. Prediction task is to determine whether a person makes over 50K a year. The original dataset had around 30,000 entries but I cut this down to 2,000 entries.</p>
        				</div>
                    </div>
                    <br/>
					<div id="tableDiv" class="table-responsive">
					</div>
					<script type="text/javascript">
						$.get('data/adultCleaned.csv', function(data) {
							var build = '<table id="dt" class="table table-hover table-condensed">\n<thead><tr> <th>Row</th> <th>Age</th> <th>Type_employer</th> <th>Education</th> <th>Marital</th> <th>Occupation</th> <th>Relationship</th> <th>Race</th> <th>Gender</th> <th>C_gain</th> <th>C_loss</th> <th>hr_per_week</th> <th>Country</th> <th>Income</th></tr></thead>\n<tbody>\n';
							var rows = data.split("\n");
							$rowNum = 1;
							rows.forEach( function getvalues(thisRow) {
								build += "<tr>\n<td>"+$rowNum+"</td>\n";
								$rowNum++;
								var columns = thisRow.split(",");
								for(var i=0;i<columns.length;i++){ build += "<td>" + columns[i] + "</td>\n"; }   			
								build += "</tr>\n";
							})
							build += "</tbody></table>";
							$('#tableDiv').append(build);
							$('#dt').DataTable({
								"order": [[ 3, "desc" ]]
				    		});	
						});
					</script>
				</div>
			<br/><br/>
			</div> <!-- Row -->


	    	<div class="row" style=" padding-top: 3%;">
           	 	<div class="col-lg-10 col-lg-offset-1"> 
		    		<div class="page-header">
	                	<h2>Data Visualisation</h2>
	            		<div class="well">
	    					<p>Dynamically created Graphs and Visualisation of the data through Rscripts that can be executed by the inputs below. This data contains factors and the graphs were built with
	    					numeric data in mind so I have added additional options for this dataset. For the single variable graph try the bar plot for the factor variables. For the two variable graphs the hex plots aren't suitable for factor data and when using the scatter plot I'd heavily advise placing the factors on the Y axis (by selecting it in the last box). </p>
	    				</div>
	                </div>
                </div>
	    	</div>

	    	<div class="row-fluid">

				<div class="col-lg-5 col-lg-offset-1" id="densityImageDiv">
					<div class="input-group">
						<label for="varTypeSelectionForOneVar" style=" margin-left: 5%;">Generate a One Variable Plot:</label>
						<select class="form-control" id="varTypeSelectionForOneVar" style=" width: 75%; margin-left: 5%;" >
								<option checked>Select type of graph</option>
								<option value="density">Density</option>
								<option value="histogram">Histogram</option>
								<option value="area">Area</option>
								<option value="bar">Bar (better for the factor data)</option>
							</select>

						<select class="form-control" id="variableSelection" style=" width: 75%; margin-left: 5%;" >
							<option checked>Select a variable</option>
							<option value="age">Age</option>
							<option value="type_employer">Employer Type</option>
							<option value="marital">Marital Status</option>
							<option value="occupation">Occupation</option>
							<option value="relationship">Relationship</option>
							<option value="race">Race</option>
							<option value="sex">Sex</option>
							<option value="capital_gain">Captial Gain</option>
							<option value="capital_loss">Captial Loss</option>
							<option value="hr_per_week">Hours per Week</option>
							<option value="country">Country</option>
						</select>
						<br/>
						<button type="submit" id="submitOneVarButton" class="btn btn-default" style="margin-left: 5%;" >Submit</button>
					</div>
					<br/>

					<img id='currentOneVarPlot' src='images/plots/oneVarPlotAdult.png' alt='' class='img-responsive' style="margin-top:10%;" />

				</div>

				<div class="col-lg-5 col-lg-offset-0" id="densityImageDiv">
					<div class="input-group">
						<label for="varTypeSelectionForTwoVar" style=" margin-left: 5%;">Generate a Two Variable Plot:</label>
						<select class="form-control" id="varTypeSelectionForTwoVar" style=" width: 75%; margin-left: 5%;" >
							<option checked>Select type of graph</option>
							<option value="scatter">Scatter</option>
							<option value="hex">Hex</option>
						</select>

						<select class="form-control" id="varSelectForTwoVar1" style=" width: 75%; margin-left: 5%;" >
							<option checked>Select a variable</option>
							<option value="age">Age</option>
							<option value="type_employer">Employer Type</option>
							<option value="marital">Marital Status</option>
							<option value="occupation">Occupation</option>
							<option value="relationship">Relationship</option>
							<option value="race">Race</option>
							<option value="sex">Sex</option>
							<option value="capital_gain">Captial Gain</option>
							<option value="capital_loss">Captial Loss</option>
							<option value="hr_per_week">Hours per Week</option>
							<option value="country">Country</option>
						</select>

						<select class="form-control" id="varSelectForTwoVar2" style=" width: 75%; margin-left: 5%;" >
							<option checked>Select a variable</option>
							<option value="age">Age</option>
							<option value="type_employer">Employer Type</option>
							<option value="marital">Marital Status</option>
							<option value="occupation">Occupation</option>
							<option value="relationship">Relationship</option>
							<option value="race">Race</option>
							<option value="sex">Sex</option>
							<option value="capital_gain">Captial Gain</option>
							<option value="capital_loss">Captial Loss</option>
							<option value="hr_per_week">Hours per Week</option>
							<option value="country">Country</option>
						</select>
						<br/>
						<button type="submit" id="submitTwoVarButton" class="btn btn-default" style="margin-left: 5%;" >Submit</button>
					</div>
					<br/>

					<img id='currentTwoVarPlot' src='images/plots/twoVarPlotAdult.png' alt='plot' class='img-responsive' class='img-responsive' style="margin-top:3%;" />

				</div>	       	
	    	</div>


	    	<div class="row" style=" padding-top: 12%;">
				<div class="col-lg-10 col-lg-offset-1"> 
					<div class="page-header">
						<h2>Prediction Algorithms</h2>
						<div class="well">
	    					<p>The available prediction algorithms and their respective R code.</p>
	    				</div>
					</div>
					<div class="col-lg-6" id="divDT" >
						<div class="panel panel-primary class" >
							<div class="panel-heading">
						    	<h3 class="panel-title">Decision Trees</h3>
						  	</div>
							<div class="panel-body">
								<textarea class="form-control" rows="15" id="DTtextArea" style=" background-color: rgb(255,255,255); border: white 0px;" ></textarea>
						 	</div>
						</div>
					</div>
	  				<div class="col-lg-6" id="divLR" >
						<div class="panel panel-primary class" >
							<div class="panel-heading">
						    	<h3 class="panel-title">Logistic Regression</h3>
						  	</div>
							<div class="panel-body" >
								<textarea class="form-control" rows="15" id="LRtextArea" style=" background-color: rgb(255,255,255); border: white 0px;" ></textarea>
						 	</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row" style=" padding-top: 3%;">
				<div class="col-lg-10 col-lg-offset-1">
					<div class="col-lg-6" id="divKNN" >
						<div class="panel panel-primary class" >
							<div class="panel-heading">
						    	<h3 class="panel-title">Nearest Neighbour</h3>
						  	</div>
							<div class="panel-body" >
								<textarea class="form-control" rows="15" id="KNNtextArea" style=" background-color: rgb(255,255,255); border: white 0px;" ></textarea>
						 	</div>
						</div>
					</div>
					<div class="col-lg-6" id="divRF" >
						<div class="panel panel-primary class" >
							<div class="panel-heading">
						    	<h3 class="panel-title">Random Forests</h3>
						  	</div>
							<div class="panel-body" >
								<textarea class="form-control" rows="15" id="RFtextArea" style=" background-color: rgb(255,255,255); border: white 0px;" ></textarea>
						 	</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row" style=" padding-top: 5%; padding-left: 1%;">
				<div class="col-lg-4 col-lg-offset-4" >
					<form action = "" method = "post">
						<div class="btn-group">
							<button type="submit" id="runScriptButton" class="btn btn-lg btn-primary">Run all methods <span class="glyphicon glyphicon-play"></span></button>
							<button type="submit" id="clearSessionButton" onclick="clearSession()" class="btn btn-lg btn-danger">Clear Session <span class="glyphicon glyphicon-remove"></span></button>
						</div>
					</form>
				</div>
			</div>

			<div class="row">
            	<div class="col-lg-3 col-lg-offset-5"> 
					 <img id='resultsLoading' src='images/loading/ring.svg' alt='' class='img-responsive' style="padding-top: 30%;" />      
                </div>
            </div>
			    
			<?php if (isset($_SESSION['adultLog'])) : ?>
			<div class="row" style=" padding-top: 2%;">
				<div class="col-md-10 col-md-offset-1"> 
					<div class="page-header">
						<h2>Adult Rscripts - Results</h2>
						<div class="well">
	    					<p>The results from executing the Rscripts is represented here through console logs, the prediction accuracy of each algorithm and generated images.</p>
	    				</div>
					</div>
				</div>
			</div>
		
			<div class="row">				
				<div class="col-md-5 col-md-offset-1">        					
					<form>
						<div class="form-group">
							<label for="logTextArea">Log:</label>
							<textarea class="form-control" rows="25" id="logTextArea"><?php  var_dump($_SESSION['adultLog']); ?></textarea>
						</div>
					</form>
				</div>

				<div class="col-md-5">
					<div id="resultsTableDiv" class="table-responsive">
					</div>
					<script type="text/javascript">
						$.get('results/adultResults.csv', function(data) {
							var build = '<table id="resultsDT" class="table table-hover">\n<thead><tr><th>Prediction Method</th> <th>Accuracy</th> </tr></thead>\n<tbody>\n';
							var rows = data.split("\n");
							rows.forEach( function getvalues(thisRow) {
								build += "<tr>\n";
								var columns = thisRow.split(",");
								if(columns[0] != ""){
									for(var i=0;i<columns.length;i++){ 
										if(isNaN(columns[i]))
											build += "<td>" + columns[i] + "</td>\n"; 
										else
											build += "<td>" + columns[i]*100 + "&#37;</td>\n"; 
									}   			
									build += "</tr>\n";
								}
							})
							build += "</tbody></table><br/>";
							$('#resultsTableDiv').append(build);	
						});
					</script>
				</div>
			</div>	
           

		    <div class="row" style=" padding-top: 3%;" > <!-- Images scroll bar -->
		    	<div class="col-lg-5 col-lg-offset-1">
		        	<div class="thumbnail">
				        <img id="myImg1" src="images/plots/adultDecisionTree.png" alt="Adult Decision Tree" style="width:100%" />
				        <div class="caption">
				          <p>The Adult Decision Tree generated by the Rscript responsible for the decision tree alogrithm. The tree is created from the training data split from the main 
				          dataset and displays the prediction logic that this method employs.</p>
				        </div>
				    </div>
  				</div>
		        <div class="col-lg-5">
		        	<div class="thumbnail">
				        <img id="myImg2" src="images/plots/adult_kPlot.png" alt="Adult Kplot" style="width:100%"  />
				        <div class="caption">
				          <p>K-means clustering of the data.</p>
				        </div>
				    </div>
  				</div>
		    </div>

		    <div class="row" style=" padding-top: 3%;" > <!-- Images scroll bar -->
		        <div class="col-lg-5 col-lg-offset-1">
		  			<div class="thumbnail">
				        <img id="myImg3" src="images/plots/adult_kMeans_CM.png" alt="Adult Kmeans Confusion Matrix" style="width:100%"  />
				        <div class="caption">
				          <p>Clustering Confusion Matrix.</p>
				        </div>
				    </div>
  				</div>
		    </div>

			<?php endif; ?>

        </div><!-- Container Fluid -->

        <footer class="footer">
      		<div class="container">
        		<!--<p class="text-muted">Dan's Project</p>-->
      		</div>
    	</footer>

	</div><!--Wrapper Div -->


	<!-- The Modal -->
	<div id="imageModal" class="modal">
		<span class="close">&times;</span>
	  	<img class="modal-content" id="imageToDisplay">
	  	<div id="caption"></div>
	</div>

</body>

<script src="js/adultSetup.js"></script>
</html>
