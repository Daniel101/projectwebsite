<?php include("header.php") ?>

        <div class="container-fluid">
            <div class="jumbotron">
				<h1 class="text-center">Dan's Project</h1>
				<p class="text-center">A project about deploying various machine learning techniques and prediction methods using the R statistical language and comparing the accuracy across different datasets.</p>
			</div>	
            <div class="row">
            	<div class="col-lg-12 col-md-offset-1">
            	    <!--<img src='images/banner.jpeg' class='img-responsive' /> -->
                </div>
            </div>

            <div class="row" >
            	<div class="col-lg-12">
            		<div class="well">
            			<h5>Pick a dataset from the menu to view the dataset. The page contains options to create dynamic graphs and the four prediction alogrithms can be executed by scrolling down to the bottom and clicking the appropriate button or through the button in the options sidebar menu on the left. The results page will run the pima and adult dataset and average the results.</h5>
            		</div>
            	</div>
            </div>

            <div class="row">
            	<div class="col-lg-4">

					<div class="panel panel-default">
						<div class="panel-heading"><center>Predictions/Machine Learning Techniques Used</center></div>					
						<ul class="list-group">
							<li class="list-group-item">
								<center><a href="">Decision Trees</a></center>
		                	</li>
							<li class="list-group-item">
	                			<center><a href="">Logistic Regression</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="">Nearest Neighbour</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="">Random Forests</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="">K-means</a></center>
	                		</li>
	                	</ul>
					</div>
	                		
                </div>
                <div class="col-lg-4">
    
                	<div class="panel panel-default">
						<div class="panel-heading"><center>Datasets Used</center></div>					
						<ul class="list-group">
							<li class="list-group-item">
								<center><a href="https://archive.ics.uci.edu/ml/datasets/Pima+Indians+Diabetes">Pima Indians Diabetes</a></center>
		                	</li>
							<li class="list-group-item">
	                			<center><a href="http://archive.ics.uci.edu/ml/datasets/Adult">Adult - Census dataset</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="http://archive.ics.uci.edu/ml/datasets/Iris">Iris Dataset</a></center>
	                		</li>
	                	</ul>
					</div>

                </div>
                <div class="col-lg-4">
                	
                	<div class="panel panel-default">
						<div class="panel-heading"><center>Demos</center></div>					
						<ul class="list-group">
							<li class="list-group-item">
								<center><a href="downloads/demos/Demo1.pptx" download>Demo 1</a>
		                		<i class="fa fa-file-powerpoint-o"></i></center>
		                	</li>
							<li class="list-group-item">
	                			<center><a href="downloads/demos/Demo2.pptx" download>Demo 2</a>
			                	<i class="fa fa-file-powerpoint-o"></i></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="downloads/demos/Demo3.pptx" download>Demo 3</a>
			                	<i class="fa fa-file-powerpoint-o"></i></center>
	                		</li>
	                	</ul>
					</div>

                </div>
            </div><!--Row -->
       
			<br/>
            <div class="row" style="padding-top: 10%">
            	<div class="col-lg-8 col-lg-offset-2">
            		<div class="well">
            			<p>Note: loading gifs and buttons for running methods can be off center at higher resoulotions, above 1920 x 1080. I will fix it when I get a chance.</p>
            		</div>
            	</div>
            </div>

        </div>
	    
	</div>
	<!--Wrapper Div -->
</body>