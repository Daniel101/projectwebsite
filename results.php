<?php include("header.php") ?>

        <div class="container-fluid">
        	<div class="row">
                <div class="col-lg-10 col-md-offset-1"> 
                	<div class="page-header">
                    	<center><h2>Results</h2></center>
                    </div>
                    <br/>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-10 col-md-offset-1"> 
                	<div class="well">
        				<p>This page gets retrieves the results of the pima and adult datasets and returns the overall average for each method. The iris dataset had been excluded because it would skew the results (but fantastic for the data visualstation).</p>
        			</div>
                </div>
            </div>

            <?php if (!isset($_SESSION['runAllScripts'])) : ?>
            <div class="row">
            	<div class="col-lg-4 col-lg-offset-4"> 
					<form action = "" method = "post"><button type="submit" id="runScriptsButton" class="btn btn-primary btn-block btn-lg"><center>Get Results</center></button></form>       
                </div>
            </div>
            <?php endif; ?>
            
            <div class="row">
            	<div class="col-lg-3 col-lg-offset-5"> 
					 <img id='resultsLoading' src='images/loading/ring.svg' alt='' class='img-responsive' style="padding-top: 40%;" />      
                </div>
            </div>
            
            <?php if (isset($_SESSION['runAllScripts'])) : ?>
            <div class="row">				
				<div class="col-lg-5 col-lg-offset-1">
					<div class="page-header">
                    	<center><h4>Pima Indian Diabetes</h4></center>
                    </div>
					<div id="resultsTableDiv" class="table-responsive">
					</div>
					<script type="text/javascript">
						$.get('results/pimaResults.csv', function(data) {
							var build = '<table id="resultsDT" class="table table-hover">\n<thead><tr><th>Prediction Method</th> <th>Accuracy</th> </tr></thead>\n<tbody>\n';
							var rows = data.split("\n");
							rows.forEach( function getvalues(thisRow) {
								build += "<tr>\n";
								var columns = thisRow.split(",");
								if(columns[0] != ""){
									for(var i=0;i<columns.length;i++){ 
										if(isNaN(columns[i]))
											build += "<td>" + columns[i] + "</td>\n"; 
										else
											build += "<td>" + columns[i]*100 + "&#37;</td>\n"; 
									}   			
									build += "</tr>\n";
								}
							})
							build += "</tbody></table><br/>";
							$('#resultsTableDiv').append(build);	
						});
					</script>
				</div>

				<div class="col-lg-5">
					<div class="page-header">
                    	<center><h4>Adult Census Dataset</h4></center>
                    </div>
					<div id="resultsTableDiv2" class="table-responsive">
					</div>
					<script type="text/javascript">
						$.get('results/adultResults.csv', function(data) {
							var build = '<table id="resultsDT" class="table table-hover">\n<thead><tr><th>Prediction Method</th> <th>Accuracy</th> </tr></thead>\n<tbody>\n';
							var rows = data.split("\n");
							rows.forEach( function getvalues(thisRow) {
								build += "<tr>\n";
								var columns = thisRow.split(",");
								if(columns[0] != ""){
									for(var i=0;i<columns.length;i++){ 
										if(isNaN(columns[i]))
											build += "<td>" + columns[i] + "</td>\n"; 
										else
											build += "<td>" + columns[i]*100 + "&#37;</td>\n"; 
									}   			
									build += "</tr>\n";
								}
							})
							build += "</tbody></table><br/>";
							$('#resultsTableDiv2').append(build);	
						});
					</script>
				</div>
			</div>

            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                	<div class="page-header">
                    	<center><h4>Overall Results</h4></center>
                    </div>
        			<div id="overallResultsTableDiv" class="table-responsive">
					</div>
					<script type="text/javascript">
						var adultResults = [];

						$.get('results/adultResults.csv', function(data) {
							var rows = data.split("\n");
							var rowNum = 0;
							rows.forEach( function getvalues(thisRow) {
								var columns = thisRow.split(",");
								if(columns[0] != ""){
									for(var i=0;i<columns.length;i++){ 
										if(isNaN(columns[i])){

										}
										else{
											adultResults[rowNum] = parseFloat(columns[i]);
										}
									}   			
									
								}
								rowNum++;

							})

								$.get('results/pimaResults.csv', function(data) {
								var build = '<table id="resultsDT" class="table table-hover">\n<thead><tr><th>Prediction Method</th> <th>Accuracy</th> </tr></thead>\n<tbody>\n';
								var rows = data.split("\n");
								var rowNum = 0;
								rows.forEach( function getvalues(thisRow) {	
									build += "<tr>\n";
									var columns = thisRow.split(",");
									if(columns[0] != ""){
										for(var i=0;i<columns.length;i++){ 
											if(isNaN(columns[i])){
												build += "<td>" + columns[i] + "</td>\n"; 
											}
											else{
												var pimaAcc = parseFloat(columns[i]);
												var adultAcc = parseFloat(adultResults[rowNum]);
												var overallAcc = ((pimaAcc+adultAcc)/2)*100;
												build += "<td>" + overallAcc + "&#37;</td>\n"; 
											}
										}   			
										build += "</tr>\n";
									}
									rowNum++;
								})
								build += "</tbody></table><br/>";
								$('#overallResultsTableDiv').append(build);	
							});
		
						});

						

					</script>	
                </div>
            </div>
            <?php endif; ?>
 
        </div>
	    
	</div>
	<!--Wrapper Div -->
</body>
