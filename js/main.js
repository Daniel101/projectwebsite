var scriptLocation;
var graphScriptLocation;
var varSelection;
var varSelectScatter1;
var varSelectScatter2;
var varTypeSelectionOneVar;
var varTypeSelectionTwoVar;


function runScript() {
      $.ajax({
           type: "POST",
           url: 'scriptRunner.php',
           data: {"scriptLocation":scriptLocation},
           success:function(html) {
             alert(html);
           }
      });
 }

 function runOneVarGraphScript() {
      $.ajax({
           type: "POST",
           url: 'graphOneVarScriptRunner.php',
           data: {"graphScriptLocation":graphScriptLocation,
                  "varTypeSelection":varTypeSelectionOneVar,
                  "varSelection":varSelection},
           success:function(html) {
              //alert(html);
              var date = new Date();
              $("#currentOneVarPlot").attr("src", "");
              if(graphScriptLocation == "pima/pimaGraph"){
                $("#currentOneVarPlot").attr("src", "images/plots/oneVarPlot.png?"+date.getTime());
              } else if(graphScriptLocation == "iris/irisGraph"){
                $("#currentOneVarPlot").attr("src", "images/plots/oneVarPlotIris.png?"+date.getTime());
              } else if(graphScriptLocation == "adult/adultGraph"){
                $("#currentOneVarPlot").attr("src", "images/plots/oneVarPlotAdult.png?"+date.getTime());
              }
              $("#currentOneVarPlot").attr("style", "margin-top:10%;");
           }
      });
 }

  function runTwoVarGraphScript() {
      $.ajax({
           type: "POST",
           url: 'graphTwoVarScriptRunner.php',
           data:{"graphScriptLocation":graphScriptLocation,
                  "varTypeSelection":varTypeSelectionTwoVar,
                  "varSelectScatter1":varSelectScatter1,
                  "varSelectScatter2":varSelectScatter2},
            success:function(html) {
            // alert(html);
              var date = new Date();
              $("#currentTwoVarPlot").attr("src", "");
              if(graphScriptLocation == "pima/pimaGraph"){
                $("#currentTwoVarPlot").attr("src", "images/plots/twoVarPlot.png?"+date.getTime());
              } else if(graphScriptLocation == "iris/irisGraph"){
                $("#currentTwoVarPlot").attr("src", "images/plots/twoVarPlotIris.png?"+date.getTime());
              } else if(graphScriptLocation == "adult/adultGraph"){
                $("#currentTwoVarPlot").attr("src", "images/plots/twoVarPlotAdult.png?"+date.getTime());
              }
              $("#currentTwoVarPlot").attr("style", "");
           }
      });
 }

  function runAllScripts() {
      $.ajax({
           type: "POST",
           url: 'runAllScripts.php',
           data:{action:'call_this'},
            success:function(html) {
            // alert(html);
            $("#resultsLoading").hide();
           }
      });
 }

function clearSession() {
      $.ajax({
           type: "POST",
           url: 'clearSession.php',
           data:{action:'call_this'},
           success:function(html) {
            alert("Session Cleared");
           }
      });
 }

function givePlotImagesModalFunc() {
  var modal = document.getElementById('imageModal');

  try{
    var plot1 = document.getElementById('myImg1');
    var plot2 = document.getElementById('myImg2');
    var plot3 = document.getElementById('myImg3');

    var modalImg = document.getElementById("imageToDisplay");
    var captionText = document.getElementById("caption");
    plot1.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    plot2.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    plot3.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }
  }
  catch(err) {
    return;
    //Dataset hasn't been run yet
    }

  var span = document.getElementsByClassName("close")[0];

  span.onclick = function() { 
      modal.style.display = "none";
  }
}

$(document).ready(function(){
	
  $(document).on("click", "#runScriptButton", function(){
    try {
		  scriptLocation = document.getElementById("scriptPage").value;
    }
    catch(err) {
      alert("Select a dataset");
      return;
    }

    if(scriptLocation != ""){
      runScript();
      $("#resultsLoading").show();
      alert("Running Rscript - " + scriptLocation);
    }
  });

    $(document).on("click", "#submitOneVarButton", function(){
      graphScriptLocation = document.getElementById("graphScriptLocation").value;
      varTypeSelectionOneVar = document.getElementById("varTypeSelectionForOneVar").value;
      varSelection = document.getElementById("variableSelection").value;
      runOneVarGraphScript();

      $("#currentOneVarPlot").attr("src", "images/loading/ring.svg");
      $("#currentOneVarPlot").attr("style", "padding-left:20%; padding-top:30%;");  
    });

    $(document).on("click", "#submitTwoVarButton", function(){
      graphScriptLocation = document.getElementById("graphScriptLocation").value;
      varTypeSelectionTwoVar = document.getElementById("varTypeSelectionForTwoVar").value;
      varSelectScatter1 = document.getElementById("varSelectForTwoVar1").value;
      varSelectScatter2 = document.getElementById("varSelectForTwoVar2").value;
      runTwoVarGraphScript();

      $("#currentTwoVarPlot").attr("src", "images/loading/ring.svg");
      $("#currentTwoVarPlot").attr("style", "padding-left:20%; padding-top:30%;");
    });

    $(document).on("click", "#runScriptsButton", function(){
      runAllScripts();
      $("#resultsLoading").show();
    });

    givePlotImagesModalFunc()
    $("#resultsLoading").hide();
});


