
$(document).ready(function(){
    $.ajax({
        url : "Rscripts/pima/pimaDecisionTree.R",
        dataType: "text",
        success : function (data) {
	       var lines = data.split("\n")
	       var para = "<p>";
	       $('#DTtextArea').append(para);
	        for (var i = 0; i < lines.length; i++) {
	            para = "<br/>"+lines[i];
	            $('#DTtextArea').append(para);
	        }
	        $('#DTtextArea').append("</p>");
        }
    });

    $.ajax({
        url : "Rscripts/pima/pimaLogisticRegression.R",
        dataType: "text",
        success : function (data) {
	       var lines = data.split("\n")
	       var para = "<p>";
	       $('#LRtextArea').append(para);
	        for (var i = 0; i < lines.length; i++) {
	            para = "<br/>"+lines[i];
	            $('#LRtextArea').append(para);
	        }
	        $('#LRtextArea').append("</p>");
        }
    });

    $.ajax({
        url : "Rscripts/pima/pimaKNN.R",
        dataType: "text",
        success : function (data) {
	       var lines = data.split("\n")
	       var para = "<p>";
	       $('KNNtextArea').append(para);
	        for (var i = 0; i < lines.length; i++) {
	            para = "<br/>"+lines[i];
	            $('#KNNtextArea').append(para);
	        }
	        $('#KNNtextArea').append("</p>");
        }
    });

    $.ajax({
        url : "Rscripts/pima/pimaRandomForest.R",
        dataType: "text",
        success : function (data) {
	       var lines = data.split("\n")
	       var para = "<p>";
	       $('#RFtextArea').append(para);
	        for (var i = 0; i < lines.length; i++) {
	            para = "<br/>"+lines[i];
	            $('#RFtextArea').append(para);
	        }
	        $('#RFtextArea').append("</p>");
        }
    });

})



