set.seed(15)
pima_rand = pima[order(runif(768)), ]

pima_train = pima_rand[1:600, ]
pima_test = pima_rand[601:768, ]

pima_train_hasDiabetes = pima_rand[1:600, 9]
pima_test_hasDiabetes= pima_rand[601:768, 9]

cat('------------------------ KNN -------------------------------')

normalize = function(x) {
  return ((x - min(x)) / (max(x) - min(x)))
}

pima_normalize = as.data.frame(lapply(pima_rand[1:8], normalize))

pima_train_norm = pima_normalize[1:600, ]
pima_test_norm = pima_normalize[601:768, ]

bestAcc = 0
bestKValue = 0
for(i in 1:30){
  pima_test_pred = knn(train = pima_train_norm, test = pima_test_norm, cl = pima_train_hasDiabetes, k=i)
  
  cm = confusionMatrix(pima_test_pred, pima_test_hasDiabetes, negative="no")
  cat('K Value = ', i, ' Accuracy = ', diagnosticErrors(cm)['acc'], ' \n')
  
  if(diagnosticErrors(cm)['acc'] > bestAcc){
    bestAcc = diagnosticErrors(cm)['acc']
    bestKValue = i
  }
}
bestAcc
bestKValue
pima_pred_KNN = knn(train = pima_train_norm, test = pima_test_norm, cl = pima_train_hasDiabetes, k=bestKValue)

CrossTable(x = pima_pred_KNN, y = pima_test_hasDiabetes, prop.chisq=FALSE,
           dnn = c('Predicted has diabetes', 'Actual has diabetes'))

cm_KNN = confusionMatrix(pima_pred_KNN, pima_test_hasDiabetes, negative="no")
diagnosticErrors(cm_KNN)['acc']
