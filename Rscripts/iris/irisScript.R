#install.packages("C50")
#install.packages("gmodels")
#install.packages("class")
#install.packages("crossval")
#install.packages("useful")
#install.packages("randomForest")
#install.packages("caret")
library(C50)
library(gmodels)
library(crossval)
library(class)
require(useful)
require(randomForest)

# ------------------------ Data Prep -------------------------------

iris=read.table("data/iris.data",sep=",",header=FALSE)
colnames(iris) = c("sepalLength", "sepalWidth", "petalLength", "petalWidth", "species")


set.seed(54)
iris_rand = iris[order(runif(150)), ]

iris_train = iris_rand[1:100, ]
iris_test = iris_rand[101:150, ]

iris_train_species = iris_rand[1:100, 5]
iris_test_species = iris_rand[101:150, 5]


# ------------------------ Call Prediction Methods -------------------------------

if(!exists("foo", mode="function")) source("Rscripts/iris/irisDecisionTree.R")
if(!exists("foo", mode="function")) source("Rscripts/iris/irisRandomForest.R")
if(!exists("foo", mode="function")) source("Rscripts/iris/irisKNN.R")
if(!exists("foo", mode="function")) source("Rscripts/iris/irisLogisticRegression.R")


# ------------------------ Comparison -------------------------------

cat('Decision Tree = ', diagnosticErrors(cm_decision_tree)['acc'], 
    '\nKNN = ', diagnosticErrors(cm_KNN)['acc'],
    '\nLogistic Regression  = ', diagnosticErrors(cm_logistic)['acc'],
    '\nRandom Forest  = ', diagnosticErrors(cm_randomForest)['acc'])

resultsData = c("Decision Tree" , diagnosticErrors(cm_decision_tree)['acc'])
resultsData = rbind(resultsData, c("KNN", diagnosticErrors(cm_KNN)['acc']))
resultsData = rbind(resultsData, c("Logistic Regression", diagnosticErrors(cm_logistic)['acc']))
resultsData = rbind(resultsData, c("Random Forests", diagnosticErrors(cm_randomForest)['acc']))
colnames(resultsData) = c("Name", "Accuracy")

write.table(resultsData, file="results/irisResults.csv", row.names=FALSE, col.names=FALSE, sep=",", quote = FALSE)