irisLog = iris
irisLog$virginica = irisLog$species == "Iris-virginica"
irisLog$species = NULL

set.seed(54)
iris_rand = irisLog[order(runif(150)), ]

iris_train = iris_rand[1:100, ]
iris_test = iris_rand[101:150, ]

iris_train_virginica = iris_rand[1:100, 5]
iris_test_virginica = iris_rand[101:150, 5]


cat('------------------------ Logistic Regression -------------------------------')


iris.glm = glm(formula=virginica ~ ., data=iris_train, family=binomial)

iris_pred_logistic = predict(iris.glm, iris_test, type="response") 

bestAcc = 0
bestThreshold = 0
predictionYN <- rep(NA, length(iris_pred_logistic))
for (threshold in seq(from=0, to=1, by=0.1)) {    
  predictionYN[iris_pred_logistic >= threshold] = 'TRUE'
  predictionYN[iris_pred_logistic < threshold] = 'FALSE'
  cm = confusionMatrix(iris_test$virginica, predictionYN, negative="FALSE")
  cat('threshold: ', threshold, 'accuracy: ', diagnosticErrors(cm)['acc'], '\n')
  
  if(diagnosticErrors(cm)['acc'] > bestAcc){
    bestAcc = diagnosticErrors(cm)['acc']
    bestThreshold = threshold
  }
}
bestThreshold

predictionYN[iris_pred_logistic >= bestThreshold] = 'TRUE'
predictionYN[iris_pred_logistic < bestThreshold] = 'FALSE'
predictionYN

CrossTable(x = predictionYN, y = iris_test_virginica, prop.chisq=FALSE,
           dnn = c('Predicted species', 'Actual species'))

cm_logistic = confusionMatrix(iris_test$virginica, predictionYN, negative="FALSE")

