library(useful)

iris=read.table("data/iris.data",sep=",",header=FALSE)
colnames(iris) = c("sepalLength", "sepalWidth", "petalLength", "petalWidth", "species")

iris$species = factor(iris$species)

set.seed(54)
iris_rand_kmeans = iris[order(runif(150)), ]

iris_train_kmeans = iris_rand_kmeans[which(names(iris_rand_kmeans) != "species")]

irisK3 = kmeans(x = iris_train_kmeans, centers = 3, nstart = 10)

# Create k-means plot as image file
plot(irisK3, data=iris_train_kmeans)
png(filename="images/plots/iris_kPlot.png", width=650, height=500)
plot(irisK3, data=iris_rand_kmeans, class="species")
dev.off()

table(iris_rand_kmeans$species, irisK3$cluster)
png(filename="images/plots/iris_kMeans_CM.png")
plot(table(iris_rand_kmeans$species, irisK3$cluster),
     main="Confusion Matrix for iris data clustering",
     xlab="Species", ylab="Cluster")
dev.off()
