set.seed(300)
adult_rand = adult[order(runif(2000)), ]

adult_train = adult_rand[1:1800, ]
adult_test = adult_rand[1801:2000, ]

adult_train_income = adult_rand[1:1800, 13]
adult_test_income = adult_rand[1801:2000, 13]

cat('------------------------ Random Forests -------------------------------')

adultForest = randomForest(income ~ ., data=adult_train)
adultForest

adult_pred_randomForest = predict(adultForest, adult_test)

result = CrossTable(adult_pred_randomForest, adult_test$income,
                    prop.chisq = FALSE, prop.c = FALSE, prop.r = FALSE,
                    dnn = c('Predicted income', 'Actual income'))

cm_randomForest = confusionMatrix(adult_pred_randomForest, adult_test_income, negative=">50K")
diagnosticErrors(cm_randomForest)['acc']