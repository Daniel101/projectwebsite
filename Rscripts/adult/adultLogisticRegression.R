set.seed(300)
adult_rand = adult[order(runif(2000)), ]

adult_train = adult_rand[1:1800, ]
adult_test = adult_rand[1801:2000, ]

adult_train_income = adult_rand[1:27162, 13]
adult_test_income = adult_rand[1801:2000, 13]

cat('------------------------ Logistic Regression -------------------------------')


adult.glm = glm(formula=income ~ ., data=adult_train, family=binomial)
adult.glm

adult_pred_logistic = predict(adult.glm, adult_test, type="response") 

bestAcc = 0
bestThreshold = 0
predictionYN <- rep(NA, length(adult_pred_logistic))
for (threshold in seq(from=0.1, to=1, by=0.1)) {    
  predictionYN[adult_pred_logistic >= threshold] = '>50K'
  predictionYN[adult_pred_logistic < threshold] = '<=50K'
  cm = confusionMatrix(adult_test$income, predictionYN, negative=">50K")
  cat('threshold: ', threshold, 'accuracy: ', diagnosticErrors(cm)['acc'], '\n')
  
  if(diagnosticErrors(cm)['acc'] > bestAcc){
    bestAcc = diagnosticErrors(cm)['acc']
    bestThreshold = threshold
  }
}
bestThreshold

predictionYN[adult_pred_logistic >= bestThreshold] = '>50K'
predictionYN[adult_pred_logistic < bestThreshold] = '<=50K'
predictionYN

CrossTable(x = predictionYN, y = adult_test_income, prop.chisq=FALSE,
           dnn = c('Predicted income', 'Actual income'))

cm_logistic = confusionMatrix(adult_test$income, predictionYN, negative=">50K")
diagnosticErrors(cm_logistic)['acc']

