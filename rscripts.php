<?php include("header.php") ?>

        <div class="container-fluid">
        	<div class="row">
                <div class="col-lg-10 col-md-offset-1"> 
                	<div class="page-header">
                    	<center><h2>Rscripts</h2></center>
                    </div>
                    <br/>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-10 col-md-offset-1"> 
                	<div class="well">
        				<p>These are the Rscripts that the website is currently running with the datasets using the R language. <a href="https://www.r-project.org/">R is a language and environment for statistical computing and graphics.</a></p>
        			</div>
                </div>
            </div>

            <div class="row">
            	<div class="col-lg-4">

            		<div class="panel panel-default">
						<div class="panel-heading"><center>Pima Indian Dataset</center></div>					
						<ul class="list-group">
							<li class="list-group-item">
								<center><a href="Rscripts/pima/pimaScript.R" >pimaScript.R</a></center>
		                	</li>
							<li class="list-group-item">
	                			<center><a href="Rscripts/pima/pimaDecisionTree.R" >pimaDecisionTree.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/pima/pimaRandomForest.R" >pimaRandomForest.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/pima/pimaKNN.R" >pimaKNN.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/pima/pimaLogisticRegression.R" >pimaLogisticRegression.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/pima/pimaK-means.R" >pimaK-means.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/pima/pimaGraphOneVar.R" >pimaGraphOneVar.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/pima/pimaGraphTwoVar.R" >pimaGraphTwoVar.R</a></center>
	                		</li>
	                	</ul>
					</div>
	                		
                </div>
                <div class="col-lg-4">
    
                	<div class="panel panel-default">
						<div class="panel-heading"><center>Adult Census Dataset</center></div>					
						<ul class="list-group">
							<li class="list-group-item">
								<center><a href="Rscripts/adult/adultScript.R" >adultScript.R</a></center>
		                	</li>
							<li class="list-group-item">
	                			<center><a href="Rscripts/adult/adultDecisionTree.R" >adultDecisionTree.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/adult/adultRandomForest.R" >adultRandomForest.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/adult/adultKNN.R" >adultKNN.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/adult/adultLogisticRegression.R" >adultLogisticRegression.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/adult/adultK-means.R" >adultK-means.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/adult/adultGraphOneVar.R" >adultGraphOneVar.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/adult/adultGraphTwoVar.R" >adultGraphTwoVar.R</a></center>
	                		</li>
	                	</ul>
					</div>

                </div>
                <div class="col-lg-4">

            		<div class="panel panel-default">
						<div class="panel-heading"><center>Iris Dataset</center></div>					
						<ul class="list-group">
							<li class="list-group-item">
								<center><a href="Rscripts/iris/irisScript.R" >irisScript.R</a></center>
		                	</li>
							<li class="list-group-item">
	                			<center><a href="Rscripts/iris/irisDecisionTree.R" >irisDecisionTree.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/iris/irisRandomForest.R" >irisRandomForest.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/iris/irisKNN.R" >irisKNN.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/iris/irisLogisticRegression.R" >irisLogisticRegression.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/iris/irisK-means.R" >irisK-means.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/iris/irisGraphOneVar.R" >irisGraphOneVar.R</a></center>
	                		</li>
	                		<li class="list-group-item">
	                			<center><a href="Rscripts/iris/irisGraphTwoVar.R" >irisGraphTwoVar.R</a></center>
	                		</li>
	                	</ul>
					</div>
	                		
                </div>

            </div>
        </div>
	    
	</div>
	<!--Wrapper Div -->
</body>
