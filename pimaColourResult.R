#install.packages("ggplot2")
#install.packages("ggthemes")
#install.packages("hexbin")
library(ggplot2)
library(ggthemes)
library(hexbin)

#args = commandArgs(trailingOnly=TRUE)

pima = read.table("data/pimaIndiansCleaned.csv",sep=",",header=FALSE)
colnames(pima) = c("numpreg", "plasmacon", "bloodpress", "skinfold", "seruminsulin", "BMI", 
                   "pedigreefunction", "age", "hasDiabetes")


normalizeForRGB = function(x) {
  return ((x - min(x)) / (max(x) - min(x)) * 255)
}


pima_rgb = as.data.frame(lapply(pima[c(6,3,2)], normalizeForRGB))
pima_hasDiabetes_label = pima[,9]




plot = ggplot(pima_rgb, aes(x = BMI ,y = bloodpress, color = pima_hasDiabetes_label, fill = pima_hasDiabetes_label, alpha=0.9)) + geom_hex(bins = 30) + theme_bw() + 
  labs(x=paste('', 'sa', sep=''),y=paste('', 'sad', sep='')) +
   theme(axis.line = element_line(size=1, colour = "black"),
         panel.grid.major = element_line(colour = "#d3d3d3"),
         panel.border = element_blank(), panel.background = element_blank(),
         axis.text.x=element_text(colour="black", size = 9),
         axis.text.y=element_text(colour="black", size = 9))

plot

#ggsave(plot, file="images/plots/scatterPlot.png")