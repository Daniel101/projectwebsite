<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
	<script src="https://code.jquery.com/jquery-1.12.4.js" integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU=" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https:////cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>

	<link href="css/simple-sidebar.css" rel="stylesheet">
    <link href="css/dans-styling.css" rel="stylesheet">
    <link href="font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
	
    <script src="js/main.js"></script> 
    <script src="js/sidebarMenu.js"></script>

	<title>Project</title>
</head>

<body>

    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">

            <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
                <li class="sidebar-brand">
                    <a class="navbar-brand" href="index.php"> Dan's Project</a>
                </li>
                <li>
                    <a href="index.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-dashboard fa-stack-1x "></i></span> Home</a>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-area-chart fa-stack-1x "></i></span> Datasets</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="pima.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-bar-chart fa-stack-1x "></i></span>Pima</a></li>
                        <li><a href="adult.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-pie-chart fa-stack-1x "></i></span>Adult - Census</a></li>
                        <li><a href="iris.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-line-chart fa-stack-1x "></i></span>Iris</a></li>
                    </ul>
                </li>
                <li>
                    <a href="rscripts.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-code fa-stack-1x "></i></span>R scripts</a>
                </li>
                <li>
                    <a href="results.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-table fa-stack-1x "></i></span>Results</a>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Options</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li>
                            <form action = "" method = "post">
                                <button type="submit" id="runScriptButton" class="btn btn-primary">Run analysis</button>
                            </form>
                        </li>    
                        <li>
                            <form action = "" method = "post">
                                <button type="submit" id="clearSessionButton" onclick="clearSession()" class="btn btn-danger">Clear Session</button>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div><!-- /#sidebar-wrapper -->
